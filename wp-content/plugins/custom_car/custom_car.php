<?php
/**
 * @package Custom Car Plugin
 * @version 1.0
 */
/*
Plugin Name: Custom Car Plugin
Plugin URI: http://radiation3.com/
Description: This plugin is used to manage Car information.
Author: Radiation 3
Version: 1.0
Author URI: http://radiation3.com/
*/



include_once ('plugin_function.php');

function custom_car_activate(){

	global $wpdb;
	$charset_collect = $wpdb->get_charset_collate();
	$table_brand = $wpdb->prefix . "brand";
	$table_model = $wpdb->prefix . "model";
	$table_engine = $wpdb->prefix . "engine";
	//$table_vehicle = $wpdb->prefix . "vehicle";
	$table_generation = $wpdb->prefix . "generation";
	$table_car_comment = $wpdb->prefix . "car_review";
	$table_car_meta = $wpdb->prefix . "car_meta";

	$sql0 = "CREATE TABLE IF NOT EXISTS `$table_brand` (
  `b_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_name` text NOT NULL,
  PRIMARY KEY (`b_id`)
) $charset_collect;";

	$sql1 = "CREATE TABLE IF NOT EXISTS `$table_model` (
  `m_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_id` text NOT NULL ,
  `m_name` text NOT NULL,
  PRIMARY KEY (`m_id`)
) $charset_collect;";

	$sql2 = "CREATE TABLE IF NOT EXISTS `$table_generation` (
`g_id` INT NOT NULL AUTO_INCREMENT , 
`b_id` int(11) NOT NULL ,
`m_id` INT NOT NULL,
`g_name` TEXT NOT NULL ,  
PRIMARY KEY (`g_id`)
) $charset_collect;";


	$sql3 = "CREATE TABLE IF NOT EXISTS `$table_engine` (
  `e_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_id` int(11) NOT NULL ,
  `m_id` int(11) NOT NULL ,
  `g_id` int(11) NOT NULL ,
  `e_name` text NOT NULL,
  `e_km_standard` TEXT  NOT NULL,
  `e_km_tuning` TEXT  NOT NULL,
  `e_nm_standard` TEXT  NOT NULL,
  `e_nm_tuning` TEXT  NOT NULL,
  `e_km_box` TEXT  NOT NULL,
  `e_nm_box` TEXT  NOT NULL,
  `e_meta_title` TEXT  NOT NULL,
  `e_meta_description` TEXT  NOT NULL,
  `e_meta_keyword` TEXT  NOT NULL,
  PRIMARY KEY (`e_id`)
) $charset_collect;";


    $custom_car_comment_table = "CREATE TABLE IF NOT EXISTS `$table_car_comment` (
      `r_id` int(11) NOT NULL AUTO_INCREMENT,
      `e_id` int NOT NULL,
      `r_title` text NOT NULL,
      `r_name` text NOT NULL,
      `r_rating` text NOT NULL,
      `r_car` text NOT NULL,
      `r_message` text NOT NULL,
      PRIMARY KEY (`r_id`)
      ) $charset_collect;";

    $custom_car_meta = "CREATE TABLE IF NOT EXISTS `$table_car_meta` (
      `meta_id` int(11) NOT NULL AUTO_INCREMENT,
      `e_id` INT NOT NULL,
      `meta_key` text NOT NULL,
      `meta_value` text NOT NULL,
      PRIMARY KEY (`meta_id`)
      ) $charset_collect;";



    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql0 );
	dbDelta( $sql1 );
	dbDelta( $sql2 );
	dbDelta( $sql3 );
	dbDelta( $custom_car_comment_table );
	dbDelta( $custom_car_meta );
}
register_activation_hook(__FILE__ , 'custom_car_activate');


function custom_car_deactivation(){

    global $wpdb;
    $table_brand = $wpdb->prefix . "brand";
    $table_model = $wpdb->prefix . "model";
    $table_engine = $wpdb->prefix . "engine";
    $table_vehicle = $wpdb->prefix . "vehicle";
    $table_generation = $wpdb->prefix . "generation";
    $table_car_comment = $wpdb->prefix . "car_review";
    $table_car_meta = $wpdb->prefix . "car_meta";

    $sql0 = "DROP TABLE `$table_brand`";

    $sql1 = "DROP TABLE `$table_model`";

    $sql2 = "DROP TABLE `$table_generation`";

    $sql3 = "DROP TABLE `$table_engine`";

    $sql4 = "DROP TABLE `$table_vehicle`";

    $sql5 = "DROP TABLE `$table_car_comment`";

    $sql6 = "DROP TABLE `$table_car_meta`";

    $wpdb->query($sql0);
    $wpdb->query($sql1);
    $wpdb->query($sql2);
    $wpdb->query($sql3);
    $wpdb->query($sql4);
    $wpdb->query($sql5);
    $wpdb->query($sql6);

}

register_deactivation_hook(__FILE__ , 'custom_car_deactivation');


add_action('admin_menu','custom_car_admin_menu');

function custom_car_admin_menu(){
	add_menu_page('Dashboard', 'Cars' , 'manage_options' , 'custom_car_main_menu' ,'custom_car_dashboard');
	add_submenu_page('custom_car_main_menu' ,'Brand', 'Brand' , 'manage_options' , 'custom_car_brand' ,'custom_car_brand' );
	add_submenu_page('custom_car_main_menu' ,'Model', 'Model' , 'manage_options' , 'custom_car_model' ,'custom_car_model' );
	add_submenu_page('custom_car_main_menu' ,'Generation', 'Generation' , 'manage_options' , 'custom_car_generation' ,'custom_car_generation' );
	add_submenu_page('custom_car_main_menu' ,'Engine', 'Engine' , 'manage_options' , 'custom_car_engine' ,'custom_car_engine' );
	add_submenu_page('#' ,'Review', 'Review' , 'manage_options' , 'custom_car_review' ,'custom_car_review' );

}

function custom_car_dashboard(){
	$page_url = get_admin_url()."admin.php?page=custom_car_brand";
	echo "<script>
			//window.location.assign('$page_url');
	</script>";

    include_once('simplexlsx.class.php');
    include('admin_include/file_import_export.php');
}


function custom_car_brand(){
	include_once ('admin_include/brand.php');
}

function custom_car_model(){
	include_once ('admin_include/model.php');
}

function custom_car_engine(){
	include_once ('admin_include/engine.php');
}
function custom_car_review(){
	include_once('admin_include/review.php');
}


function custom_car_generation(){
	include_once ('admin_include/generation.php');
}


//add_action('wp_enqueue_scripts' , 'custom_car_style');
add_action('admin_enqueue_scripts' , 'custom_car_style');

function custom_car_style(){
	wp_enqueue_style('custom_car_style', plugin_dir_url(__FILE__).'assets/custom_car.css');
	//wp_enqueue_style('custom_bootstrap', plugin_dir_url(__FILE__).'bootstrap/css/bootstrap.css');
}



add_action( 'wp_ajax_get_model_by_brand', 'get_model_by_brand' );
add_action( 'wp_ajax_nopriv_get_model_by_brand', 'get_model_by_brand' );
function get_model_by_brand() {
    global $wpdb; // this is how you get access to the database
    $b_id = intval( $_POST['brand_id'] );
    $table_engine  = $wpdb->prefix .'engine';
    //$models = $wpdb->get_results("SELECT DISTINCT `m_id` FROM $table_engine WHERE `b_id` = $b_id " , true );
    $models = get_all_model($b_id);
    $model_new = array();
    foreach ($models as $model){
        $model_new[get_model_name_by_id($model->m_id)] = $model->m_id;
    }
    print_r(json_encode($model_new));
}


add_action( 'wp_ajax_get_generation_by_brand_and_model', 'get_generation_by_brand_and_model' );
add_action( 'wp_ajax_nopriv_get_generation_by_brand_and_model', 'get_generation_by_brand_and_model' );
function get_generation_by_brand_and_model()
{
    global $wpdb; // this is how you get access to the database
    $b_id =  $_POST['brand_id'] ;
    $m_id =  $_POST['model_id'];
    $table_engine  = $wpdb->prefix .'engine';
    $generations = $wpdb->get_results("SELECT  * FROM $table_engine WHERE `b_id` = $b_id AND `m_id` = $m_id " , true );
    $generation_new = array();
    foreach ($generations as $generation){
        $generation_new[get_generation_name_by_id($generation->g_id)] = $generation->g_id;
    }
    print_r(json_encode($generation_new));
}


add_action( 'wp_ajax_get_engine_by_brand_model_and_generation', 'get_engine_by_brand_model_and_generation' );
add_action( 'wp_ajax_nopriv_get_engine_by_brand_model_and_generation', 'get_engine_by_brand_model_and_generation' );
function get_engine_by_brand_model_and_generation() {
    global $wpdb; // this is how you get access to the database
    $b_id =  $_POST['brand_id'] ;
    $m_id =  $_POST['model_id'];
    $g_id =  $_POST['generation_id'];
    $table_engine  = $wpdb->prefix .'engine';
    $engines = $wpdb->get_results("SELECT DISTINCT `e_id` FROM $table_engine WHERE `b_id` = $b_id AND `m_id` = $m_id  AND `g_id` = $g_id" , true );
    $engine_new = array();
    foreach ($engines as $engine){
        $engine_new[get_engine_name_by_id($engine->e_id)] = $engine->e_id;
    }
    print_r(json_encode($engine_new));
}

add_action( 'wp_ajax_get_engine_details_by_brand_model_and_generation_and_engine', 'get_engine_details_by_brand_model_and_generation_and_engine' );
add_action( 'wp_ajax_nopriv_get_engine_details_by_brand_model_and_generation_and_engine', 'get_engine_details_by_brand_model_and_generation_and_engine' );
function get_engine_details_by_brand_model_and_generation_and_engine() {

    global $wpdb; // this is how you get access to the database
    $b_id =  $_POST['brand_id'] ;
    $m_id =  $_POST['model_id'];
    $g_id =  $_POST['generation_id'];
    $e_id =  $_POST['engine_id'];
    $table_engine  = $wpdb->prefix .'engine';
    $engines = $wpdb->get_results("SELECT  * FROM $table_engine WHERE `b_id` = $b_id AND `m_id` = $m_id  AND `g_id` = $g_id AND `e_id` = $e_id" , true );
    print_r(json_encode($engines));

   }


function show_main_page(){
  include_once ("client_include/main.php");
}
add_shortcode('custom_car', 'show_main_page');



add_action( 'wp_enqueue_scripts', 'ajax_test_enqueue_scripts' );
function ajax_test_enqueue_scripts() {
    wp_enqueue_script( 'test', plugins_url( '/test.js', __FILE__ ), array('jquery'), '1.0', true );
}


