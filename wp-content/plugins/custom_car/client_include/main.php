<?php
/**
 * Created by PhpStorm.
 * User: SHAN
 * Date: 8/7/2016
 * Time: 6:43 PM
 */

$all_brands = get_all_brand();
?>

    <style>
        select:disabled {
            background: #dddddd;
        }
        h1 {
            color: #EEEEEE;
        }
    </style>


    <link type="text/css" rel="stylesheet" href="<?php echo plugins_url();?>/custom_car/assets/waitMe.css">
    <script src="<?php echo plugins_url();?>/custom_car/assets/waitMe.js"></script>

    <script>
        jQuery('#loader-wrapper').waitMe({
            effect : 'win8_linear',
            text : 'Loading...',
            bg : '#CCC',
            color : '#000',
            bg: 'rgba(255,255,255,0.5)',
            maxSize : '100%',
            textPos : 'horizontal',
            opacity:0.5,
            source : ''
        });
    </script>
<div style="text-align: center" id="loader-wrapper" class="custom-div waitMe_container">
    <form name="custom_car_form"  class="custom_car_form" id="custom_car_form" method="get">
        <select class="b_id" name="b_id">
            <option value=""> Select Brand</option>
            <?php foreach ( $all_brands as $brand ) {?>
                <option value="<?php echo $brand->b_id ;?>"> <?php echo $brand->b_name ;?> </option>
            <?php } ?>
        </select>
        <select class="search m_id" name="m_id" disabled>
            <option value=""> Select Model</option>
        </select>
        <select class="search g_id " name="g_id" disabled>
            <option value=""> Select Generation</option>
        </select>
        <select class="search e_id " name="e_id" disabled>
            <option value=""> Select Engine</option>
        </select>
        <button type="submit" name="car_search" class="search result_btn ">Search</button>
        <!--<div class="search result hidden"></div>-->
        <div class="search loading"></div>
        <input class="ajax_path" value="<?php echo admin_url('admin-ajax.php');  ?>" type="hidden">
    </form>
</div>

<?php
if(isset($_GET['car_search'])){
    global $wpdb;
    $b_id = $_GET['b_id'];
    $m_id = $_GET['m_id'];
    $g_id = $_GET['g_id'];
    $e_id = $_GET['e_id'];


    $table_review = $wpdb->prefix .'car_review';
    $table_engine = $wpdb->prefix .'engine';
    $all_reviews = $wpdb->get_results("SELECT * FROM $table_review WHERE e_id = $e_id");
    $all_engine = $wpdb->get_results("SELECT * FROM $table_engine WHERE e_id = $e_id");

    $_SESSION['current_engine_details'] = $all_engine;


    function add_meta_tags() {
            $all_engine =  $_SESSION['current_engine_details'];
            echo "<meta name='description' content='".$all_engine[0]->e_meta_description."' />" . "\n";
            echo "<meta name='keywords' content='".$all_engine[0]->e_meta_keyword."' />" . "\n";
            echo "<meta name='title' content='".$all_engine[0]->e_meta_title."' />" . "\n";
    }
    add_action( 'wp_head', 'add_meta_tags' , 1000 , 1 );
    wp_head();

    $_SESSION['current_engine_details'] = "";

    ?>

    <script>
        jQuery('head').append('<meta name="description" content="<?=$all_engine[0]->e_meta_description?>" />');
        jQuery('head').append('<meta name="keywords" content="<?=$all_engine[0]->e_meta_keyword?>" />');
        jQuery('head').append('<meta name="title" content="<?=$all_engine[0]->e_meta_title?>" />');
    </script>


<!-- This Is the Table For Engine Details -->

    <style>
        .col-md-4 {
            background-color: white;
        }
    </style>
    <h2>Engine Details</h2>

                <div class="col-md-4">
                    <h2> <label> Brand </label> </h2>
                    <p>
                        <?php echo get_brand_name_by_id($all_engine[0]->b_id) ?>
                    </p>
                </div>
                <div class="col-md-4">
                    <h2> <label> Model </label> </h2>
                    <p>
                            <?php echo get_model_name_by_id($all_engine[0]->m_id) ?>
                    </p>
                </div>
                <div class="col-md-4">

                    <h2> <label> Generations </label> </h2>
                    <p>

                        <?php echo get_generation_name_by_id($all_engine[0]->g_id) ?>

                    </p>

                </div>
                <div class="col-md-4">
                    <h2><label>engine Name</label></h2>
                    <p>
                        <?php echo get_engine_name_by_id($all_engine[0]->e_id) ?>
                    </p>
                </div>
                <div class="col-md-4">

                    <h2><label>KM tuning</label></h2>
                    <p>
                        <?php echo $all_engine[0]->e_km_tuning ?>
                    </p>
                </div>
                <div class="col-md-4">

                    <h2><label>Km box</label></h2>
                    <p>
                        <?php echo $all_engine[0]->e_km_box ?>
                    </p>
                </div>
                <div class="col-md-4">
                    <h2><label>KM standard</label></h2>
                    <p>
                       <?php echo $all_engine[0]->e_km_standard ?>
                    </p>

                </div>
                <div class="col-md-4">

                    <h2><label>NM tuning</label></h2>
                    <p>
                        <?php echo $all_engine[0]->e_nm_tuning ?>
                    </p>

                </div>
                <div class="col-md-4">
                    <h2><label>NM box</label></h2>
                    <p>
                        <?php echo $all_engine[0]->e_nm_box ?>
                    </p>


                </div>
                <div class="col-md-4">

                    <h2><label>NM standard</label></h2>
                    <p>
                        <?php echo $all_engine[0]->e_nm_standard ?>
                    </p>
                </div>


    <div class="col-md-4">
        <p>&nbsp;</p>
    </div>
    <div class="col-md-4">
        <p>&nbsp;</p>
    </div>
    <div class="clear-fix"></div>
    <!-- This Is the Table For Review Details -->
<h2> Reviews Table </h2>
    <table width="100%">
        <tr>
            <th>Review Title</th> <th> Review  Name </th> <th> Review Rating </th> <th>Review Car</th> <th>Message</th>
        </tr>
        <?php foreach($all_reviews as $review_row){?>
            <tr>
                <td><?php echo $review_row->r_title?></td>
                <td> <?php echo $review_row->r_name ?></td>
                <td> <?php echo $review_row->r_rating ?></td>
                <td><?php echo $review_row->r_car ?></td>
                <td><?php echo $review_row->r_message ?></td>
            </tr>
        <?php } ?>
    </table>
<?php } ?>