<?php
/**
 * Created by PhpStorm.
 * User: SHAN
 * Date: 7/19/2016
 * Time: 11:22 PM
 */
?>
<?php
global $wpdb;
$all_brand = get_all_brand();
$page_url = get_admin_url()."admin.php?page=custom_car_model";
$table_model = $wpdb->prefix .'model';
if(isset($_POST['insert_model'])):
	$data_array = array(
		'b_id' => $_POST['brand_id'],
		'm_name' => $_POST['model_name'],
	);
	$wpdb->insert($table_model ,$data_array);
	echo "<script>
			window.location.assign('$page_url');
		</script>";
endif;

if(isset($_GET['custom_car_model_delete'])):
	$data_array = array(
		'm_id' => $_GET['custom_car_model_id'],
	);
	$wpdb->delete($table_model , $data_array);
	echo "<script>
			window.location.assign('$page_url');
		</script>";
endif;

if(isset($_GET['custom_car_model_edit']) && isset($_GET['custom_car_model_id'])){
	$_model_id = $_GET['custom_car_model_id'];
	$data_array = array(
		'm_id' => $_model_id,
	);
	$all_model = $wpdb->get_results("SELECT * FROM $table_model WHERE m_id = $_model_id " , true );
}

if(isset($_POST['update_model'])):
	$data_array = array(
		'b_id' => $_POST['brand_id'],
		'm_name' => $_POST['model_name']
	);
	$where = array(
		'm_id' => $_POST['model_id']
	);
	$wpdb->update( $table_model , $data_array, $where);
	echo "<script>
			window.location.assign('$page_url');
		</script>";
endif;

$all_models = $wpdb->get_results("SELECT * FROM $table_model");

?>
<div class="custom_car_container">
<?php if(isset($_GET['custom_car_model_edit']) && isset($_GET['custom_car_model_id'])){ ?>
	<form id="custom-car-model" method="post">
		<select name="brand_id">

			<?php
			foreach($all_brand as $brand){
				$selected = "";
				if($all_model[0]->b_id == $brand->b_id){
				$selected = " SELECTED ";
				}
				?>
				<option value="<?php echo $brand->b_id; ?>" <?php echo $selected; ?>> <?php echo $brand->b_name?></option>
			<?php } ?>
		</select>

		<input type="text" name="model_name" value="<?php echo $all_model[0]->m_name ?>">
		<input type="hidden" name="model_id" value="<?php echo $all_model[0]->m_id ?>">
		<button type="submit" name="update_model">Update model</button>
	</form>
<?php }else { ?>

	<form id="custom-car-model" method="post">
		<select name="brand_id">
			<option> Select Brand </option>
		<?php
		foreach($all_brand as $brand){ ?>
			<option value="<?php echo $brand->b_id; ?>"> <?php echo $brand->b_name?></option>
		<?php } ?>
		</select>
		<input type="text" name="model_name" placeholder="Model Name">
		<button type="submit" name="insert_model">Add model</button>
	</form>

<?php } ?>

<table width="100%">
	<tr>
		<th> model Id</th> <th> Brand Name </th> <th> model Name</th><th> Actions </th>
	</tr>
	<?php foreach($all_models as $model_row){?>
		<tr>
			<td><?php print_r($model_row->m_id) ?></td><td><?php echo get_brand_name_by_id($model_row->b_id) ?><td><?php print_r($model_row->m_name) ?></td><td> <a href="<?php echo get_admin_url().'admin.php?page=custom_car_model&custom_car_model_edit=yes&custom_car_model_id='.$model_row->m_id ?>">Edit</a> | <a href="<?php echo get_admin_url().'admin.php?page=custom_car_model&custom_car_model_delete=yes&custom_car_model_id='.$model_row->m_id ?>">Delete</a> </td>
		</tr>
	<?php } ?>
</table>
</div>

