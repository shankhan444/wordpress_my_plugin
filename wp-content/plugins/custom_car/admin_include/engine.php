<?php
/**
 * Created by PhpStorm.
 * User: SHAN
 * Date: 7/19/2016
 * Time: 11:22 PM
 */
?>

<style>

    #loader-wrapper{
        display: none;
    }

    #loader-wrapper .loader-section {
        position: fixed;
        top: 0;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0.58);;
        z-index: 10000;
    }

    #loader {
        z-index: 1001; /* anything higher than z-index: 1000 of .loader-section */
    }

    h1 {
        color: #EEEEEE;
    }

    #loader-wrapper{
        color: white;
    }

    #loader.loader-section img{
        display: block;
        margin-left: auto;
        margin-right: auto;
        margin: 0 auto;
    }

    .loader-section img{

        margin-top: 250px;
        /*
        margin-left: 40%;
        */
        height: 200px;
        width: 200px;
    }

</style>


<div id="loader-wrapper">
    <div id="loader">
        <div class="loader-section">
            <center>
                <img src="<?php echo plugins_url() ?>/custom_car/load.gif">
            </center>

        </div>
    </div>


</div>


<script>
    jQuery('#loader-wrapper').show();
</script>


<?php
global $wpdb;
$page_url = get_admin_url()."admin.php?page=custom_car_engine";
$table_engine = $wpdb->prefix .'engine';
$all_brand = get_all_brand();
$all_engine = get_all_engine();

if(isset($_POST['insert_engine'])):
    $data_array = array(
        'e_name' => $_POST['engine_name'],
        'b_id' => $_POST['brand_id'],
        'm_id' => $_POST['model_id'],
        'g_id' => $_POST['generation_id'],
        'e_km_tuning' => $_POST['e_km_tuning'],
        'e_km_standard' => $_POST['e_km_standard'],
        'e_km_box' => $_POST['e_km_box'],
        'e_nm_tuning' => $_POST['e_nm_tuning'],
        'e_nm_standard' => $_POST['e_nm_standard'],
        'e_nm_box' => $_POST['e_nm_box'],
        'e_meta_description' => $_POST['e_meta_description'],
        'e_meta_keyword' => $_POST['e_meta_keyword'],
        'e_meta_title' => $_POST['e_meta_title'],

    );

    $wpdb->insert($table_engine ,$data_array);
    echo "<script>
			window.location.assign('$page_url');
		</script>";
endif;

if(isset($_GET['custom_car_engine_delete'])):
    $data_array = array(
        'e_id' => $_GET['custom_car_engine_id'],
    );
    $wpdb->delete($table_engine , $data_array);
    echo "<script>
			window.location.assign('$page_url');
		</script>";
endif;

if(isset($_GET['custom_car_engine_edit']) && isset($_GET['custom_car_engine_id'])){
    $_engine_id = $_GET['custom_car_engine_id'];
    $all_engine = $wpdb->get_results("SELECT * FROM $table_engine WHERE e_id = $_engine_id " , true );
    $all_model = get_all_model($all_engine[0]->b_id);
    $all_generation = get_all_generation($all_engine[0]->b_id , $all_engine[0]->m_id);
}

if(isset($_POST['update_engine'])):
    $data_array = array(
        'e_name' => $_POST['engine_name'],
        'b_id' => $_POST['brand_id'],
        'm_id' => $_POST['model_id'],
        'g_id' => $_POST['generation_id'],
        'e_id' => $_POST['engine_id'],
        'e_km_tuning' => $_POST['e_km_tuning'],
        'e_km_standard' => $_POST['e_km_standard'],
        'e_km_box' => $_POST['e_km_box'],
        'e_nm_tuning' => $_POST['e_nm_tuning'],
        'e_nm_standard' => $_POST['e_nm_standard'],
        'e_nm_box' => $_POST['e_nm_box'],
        'e_meta_description' => $_POST['e_meta_description'],
        'e_meta_keyword' => $_POST['e_meta_keyword'],
        'e_meta_title' => $_POST['e_meta_title'],
    );
    $where = array(
        'e_id' => $_POST['engine_id']
    );
    $wpdb->update( $table_engine , $data_array, $where);
    echo "<script>
			window.location.assign('$page_url');
		</script>";
endif;

$all_engines = $wpdb->get_results("SELECT * FROM $table_engine");

?>
<style>
    .custom_car_container tr:nth-child(odd) {
        background: none;
    }

    .custom_car_container select ,   .custom_car_container input{
        padding: 2px;
        line-height: 28px;
        height: 28px;
        width: 100%;
    }
</style>
<div class="custom_car_container">
<?php if(isset($_GET['custom_car_engine_edit']) && isset($_GET['custom_car_engine_id'])){ ?>
    <form id="custom-car-engine" method="post">
        <input type="hidden" name="engine_id" value="<?php echo $all_engine[0]->e_id ?>">
        <table>
<!--            <tr><th>Model</th><th>Generation</th></tr>-->
            <tr>
                <th> <label> Brand </label></th>
                <td>
                    <select name="brand_id" id="brand_id" required>
                        <option value="<?php echo $all_engine[0]->b_id?>"> <?php echo get_brand_name_by_id($all_engine[0]->b_id) ?></option>
                        <?php foreach ($all_brand as $brand){?>
                            <option value="<?php echo $brand->b_id?>"> <?php echo $brand->b_name ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>

            <tr>
                <th> <label> Model </label> </th>
                <td>
                    <select name="model_id" id="model_id" required>
                        <option value="<?php echo $all_engine[0]->m_id?>"> <?php echo get_model_name_by_id($all_engine[0]->m_id) ?></option>
                        <?php foreach ($all_model as $model){?>
                            <option value="<?php echo $model->m_id?>"> <?php echo get_model_name_by_id($model->m_id) ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>

            <tr>
                <th> <label> Generations </label> </th>
                <td>
                    <select name="generation_id" id="generation_id" required>
                        <option value="<?php echo $all_engine[0]->g_id?>"> <?php echo get_generation_name_by_id($all_engine[0]->g_id) ?></option>
                        <?php foreach ($all_generation as $generation){?>
                            <option value="<?php echo $generation->g_id?>"> <?php echo get_generation_name_by_id($generation->g_id) ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            <!--
            <tr>
                <th> <label> Engine </label> </th>
                <td>
                    <input  type="hidden" value="<?php echo $all_engine[0]->e_id; ?>">
                </td>
            </tr>
            -->

            <tr>
                <th><label>engine Name</label></th>
                <td>
                    <input type="text" name="engine_name" required value="<?php echo get_engine_name_by_id($all_engine[0]->e_id) ?>">
                </td>
            </tr>
            <tr>
                <th><label>KM tuning</label></th>
                <td>
                    <input type="text" name="e_km_tuning" required value="<?php echo $all_engine[0]->e_km_tuning ?>">
                </td>
            </tr>
            <tr>
                <th><label>Km box</label></th>
                <td>
                    <input type="text" name="e_km_box" required value="<?php echo $all_engine[0]->e_km_box ?>">
                </td>
            </tr>
            <tr>
                <th><label>KM standard</label></th>
                <td>
                    <input type="text" name="e_km_standard" required value="<?php echo $all_engine[0]->e_km_standard ?>">
                </td>
            </tr>
            <tr>
                <th><label>NM tuning</label></th>
                <td>
                    <input type="text" name="e_nm_tuning" required value="<?php echo $all_engine[0]->e_nm_tuning ?>">
                </td>
            </tr>
            <tr>
                <th><label>NM box</label></th>
                <td>
                    <input type="text" name="e_nm_box" required value="<?php echo $all_engine[0]->e_nm_box ?>">
                </td>
            </tr>
            <tr>
                <th><label>NM standard</label></th>
                <td>
                    <input type="text" name="e_nm_standard" required value="<?php echo $all_engine[0]->e_nm_standard ?>">
                </td>
            </tr>
            <tr>
                <th><label>Meta Title</label></th>
                <td>
                    <input type="text" name="e_meta_title" placeholder="Title" value="<?php echo $all_engine[0]->e_meta_title ?>" >
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    keywords e.g. BMW 7 series, BMW, Cheap BMW
                </td>
            </tr>

            <tr>
                <th><label>Meta Keyword</label></th>
                <td>
                    <input type="text" name="e_meta_keyword"  value="<?php echo $all_engine[0]->e_meta_keyword ?>" placeholder="Engine Name , Model Name , Brand Name ">
                </td>
            </tr>

            <tr>
                <th><label>Meta Description</label></th>
                <td>
                    <input type="text" name="e_meta_description"  value="<?php echo $all_engine[0]->e_meta_description ?>">
                </td>
            </tr>

        </table>
        <button type="submit" name="update_engine">Update engine</button>
    </form>
<?php }

else { ?>
    <form id="custom-car-engine" method="post">
        <table>
            <tr>
                <th><label>Brand</label></th>
                <td>
                    <select name="brand_id" id="brand_id" required>
                        <option value=""> Select Brand</option>
                        <?php foreach ($all_brand as $brand){?>
                            <option value="<?php echo $brand->b_id?>"> <?php echo $brand->b_name ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <th><label>Model</label></th>
                <td>
                    <select name="model_id" id="model_id" required></select>
                </td>
            </tr>
            <tr>
                <th><label>Generation</label></th>
                <td>
                    <select name="generation_id" id="generation_id" required></select>
                </td>
            </tr>
            <tr class="engine_details_row">
                <th><label>Engine Name</label></th>
                <td>
                    <input type="text" name="engine_name" required>
                </td>
            </tr>
            <tr class="engine_details_row">
                <th><label>KM TUNING</label></th>
                <td>
                    <input type="text" name="e_km_tuning" required>
                </td>
            </tr>
            <tr class="engine_details_row">
                <th><label>KM BOX</label></th>
                <td>
                    <input type="text" name="e_km_box" required>
                </td>
            </tr>
            <tr class="engine_details_row">
                <th><label>KM STANDARD</label></th>
                <td>
                    <input type="text" name="e_km_standard" required>
                </td>
            </tr>
            <tr class="engine_details_row">
                <th><label>NM TUNING</label></th>
                <td>
                    <input type="text" name="e_nm_tuning" required>
                </td>
            </tr>
            <tr class="engine_details_row">
                <th><label>NM BOX</label></th>
                <td>
                    <input type="text" name="e_nm_box" required>
                </td>
            </tr>
            <tr class="engine_details_row">
                <th><label>NM STANDARD</label></th>
                <td>
                    <input type="text" name="e_nm_standard" required>
                </td>
            </tr>
            <tr>
                <th><label>Meta Title</label></th>
                <td>
                    <input type="text" name="e_meta_title" placeholder="Title">
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    keywords e.g. BMW 7 series, BMW, Cheap BMW
                </td>
            </tr>
            <tr>
                <th><label>Meta Keyword</label></th>
                <td>
                    <input type="text" name="e_meta_keyword" placeholder="Engine Name , Model Name , Brand Name ">
                </td>
            </tr>

            <tr>
                <th><label>Meta Description</label></th>
                <td>
                    <input type="text" name="e_meta_description" placeholder="Add Description for SEO " >
                </td>
            </tr>
            <tr class="engine_details_row">
                <td colspan="2"> <button type="submit" name="insert_engine">Add Engine</button></td>
            </tr>
        </table>

    </form>
<?php } ?>
    <input class="ajax_path" value="<?php echo admin_url('admin-ajax.php');  ?>" type="hidden">
<table width="100%">
    <tr>
        <th>engine Id</th><th> Brand </th><th>Model</th> <th> Generation</th> <th> Engine </th> <th> engine Name</th>  <th>Action</th>
    </tr>
    <?php foreach($all_engines as $engine_row){?>
        <tr>
            <td><?php print_r($engine_row->e_id) ?></td>
            <td><?php echo get_brand_name_by_id($engine_row->b_id) ?></td>
            <td><?php echo get_model_name_by_id($engine_row->m_id)?></td>
            <td> <?php echo get_generation_name_by_id($engine_row->g_id) ?></td>
            <td> <?php echo get_engine_name_by_id($engine_row->e_id) ?></td>
            <td><?php echo get_engine_name_by_id($engine_row->e_id) ?></td>
            <td> <a href="<?php echo get_admin_url().'admin.php?page=custom_car_engine&custom_car_engine_edit=yes&custom_car_engine_id='.$engine_row->e_id ?>">Edit</a> | <a href="<?php echo get_admin_url().'admin.php?page=custom_car_engine&custom_car_engine_delete=yes&custom_car_engine_id='.$engine_row->e_id ?>">Delete</a> | <a href="<?php echo get_admin_url().'admin.php?page=custom_car_review&custom_car_engine_review=yes&custom_car_engine_id='.$engine_row->e_id ?>" target="_blank">Review</a> </td>
        </tr>
    <?php } ?>
</table>
</div>




<script>

    jQuery(document).ready(function ($) {
        $('#loader-wrapper').hide();

        $('#brand_id').change(function () {
            $('#model_id').empty();
            $('#generation_id').empty();
            //$('#generation_name').addClass('hidden');
            if($('#brand_id').val() == "")
                return ;

            var data = {
                'action': 'get_model_by_brand',
                'brand_id': $('#brand_id').val()     // We pass php values differently!
            };
            $.ajax({
                url : $('.ajax_path').val(),
                method : 'post',
                data : data ,
                success : function (response) {
                    console.log(response);

                    response = response.slice(0,-1);
                    console.log(response);
                    response = JSON.parse(response);

                    //console.log(response);
                    $('#model_id').append('<option value=""> Select Model</option>');
                    $.each(response , function (model_name , model_index) {
                        $('#model_id').append('<option value="'+model_index+'">' + model_name +'</option>')
                    });

                }
            });
        });
        $('#model_id').change(function (){
                $('#generation_id').empty();
                if($('#model_id').val() == "")
                    return ;
                var data = {
                    'action': 'get_generation_by_brand_and_model',
                    'brand_id': $('#brand_id').val(),     // We pass php values differently!
                    'model_id': $('#model_id').val()     // We pass php values differently!
                };
                $.ajax({
                    url : $('.ajax_path').val(),
                    method : 'post',
                    data : data ,
                    success : function (response) {
                        console.log(response);

                        response = response.slice(0,-1);
                        console.log(response);

                        response = JSON.parse(response);
                        //console.log(response);
                        $('#generation_id').append('<option value=""> Select Generation </option>');
                        $.each(response , function (generation_name , generation_index) {
                            $('#generation_id').append('<option value="'+generation_index+'">' + generation_name +'</option>')
                        });
                    }
                });


        });

    });
</script>