<?php
/**
 * Created by PhpStorm.
 * User: SHAN
 * Date: 7/19/2016
 * Time: 11:22 PM
 */
?>
<?php
	global $wpdb;
	$page_url = get_admin_url()."admin.php?page=custom_car_brand";
	$table_brand = $wpdb->prefix .'brand';
	if(isset($_POST['insert_brand'])):
		$data_array = array(
			'b_name' => $_POST['brand_name'],
		);
		$wpdb->insert($table_brand ,$data_array);
		echo "<script>
			window.location.assign('$page_url');
		</script>";
	endif;
	if(isset($_GET['custom_car_brand_delete'])):
		$data_array = array(
			'b_id' => $_GET['custom_car_brand_id'],
		);
		$wpdb->delete($table_brand , $data_array);
		echo "<script>
			window.location.assign('$page_url');
		</script>";
	endif;
	 if(isset($_GET['custom_car_brand_edit']) && isset($_GET['custom_car_brand_id'])){
	    $_brand_id = $_GET['custom_car_brand_id'];
		 $data_array = array(
			 'b_id' => $_brand_id,
		 );
		$all_brand = $wpdb->get_results("SELECT * FROM $table_brand WHERE b_id = $_brand_id " , true );
	 }
	 if(isset($_POST['update_brand'])):
		 $data_array = array(
		 	'b_name' => $_POST['brand_name']
		 );
		 $where = array(
			'b_id' => $_POST['brand_id']
		 );
		 $wpdb->update( $table_brand , $data_array, $where);
		 echo "<script>
			window.location.assign('$page_url');
		</script>";
     endif;
	$all_brands = $wpdb->get_results("SELECT * FROM $table_brand");
?>


<div class="custom_car_container">
	<?php if(isset($_GET['custom_car_brand_edit']) && isset($_GET['custom_car_brand_id'])){ ?>
		<form id="custom-car-brand" method="post">
			<input type="text" name="brand_name" value="<?php echo $all_brand[0]->b_name ?>">
			<input type="hidden" name="brand_id" value="<?php echo $all_brand[0]->b_id ?>">
			<button type="submit" name="update_brand">Update Brand</button>
		</form>
	<?php }else { ?>

		<form id="custom-car-brand" method="post">
			<input type="text" name="brand_name">
			<button type="submit" name="insert_brand">Add Brand</button>
		</form>

	<?php } ?>

	<table width="100%">
		<tr>
			<th> Brand Id</th><th> Brand Name</th><th> Actions </th>
		</tr>
		<?php foreach($all_brands as $brand_row){?>
			<tr>
				<td><?php print_r($brand_row->b_id) ?></td><td><?php print_r($brand_row->b_name) ?></td><td> <a href="<?php echo get_admin_url().'admin.php?page=custom_car_brand&custom_car_brand_edit=yes&custom_car_brand_id='.$brand_row->b_id ?>">Edit</a> | <a href="<?php echo get_admin_url().'admin.php?page=custom_car_brand&custom_car_brand_delete=yes&custom_car_brand_id='.$brand_row->b_id ?>">Delete</a> </td>
			</tr>
		<?php } ?>
	</table>


</div>

