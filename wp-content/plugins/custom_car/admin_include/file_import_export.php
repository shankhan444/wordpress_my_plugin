<?php
/**
 * Created by PhpStorm.
 * User: SHAN
 * Date: 8/3/2016
 * Time: 9:28 PM
 */

if(isset($_POST['upload_xslx'])) {

    if (!function_exists('wp_handle_upload')) {
        require_once(ABSPATH . 'wp-admin/includes/file.php');
    }

    $uploadedfile = $_FILES['upload_sheet'];

    $upload_overrides = array('test_form' => false);

    $movefile = wp_handle_upload($uploadedfile, $upload_overrides);

    if ($movefile && !isset($movefile['error'])) {
        // echo "File Uploaded Successfully";

        //var_dump($movefile);

        $file_name = $movefile['file'];

        $file_ext = strtolower(substr($file_name, strrpos($file_name, '.') + 1));

        if ($file_ext == "xlsx") {
            $xlsx = new SimpleXLSX($file_name);
            echo "<table>";
            $i = 0;
            $array_of_all_file = $xlsx->rows();
            $array_of_all_file = array_unique($array_of_all_file , SORT_REGULAR);
            $all_brand = array();
            $all_model = array();
            $all_generation = array();
            $all_engine = array();
            $j = 0;
            foreach ($array_of_all_file as $data) {
                if($j != 0){
                    /*  0 -> Full name
                        1 -> Model
                        2 -> Generation
                        3 -> Engine
                        4 -> Brand
                        5 -> KM standard
                        6 -> KM Tuning
                        7 -> NM standard
                        8 -> NM Tuning
                        9 -> KM Box
                        10-> NM Box
                    */
                    array_push( $all_brand ,$data[1]);
                    array_push( $all_model ,array($data[1] , $data[2]));
                    array_push( $all_generation , array($data[1] , $data[2] ,$data[3]));
                    array_push( $all_engine , $data[4]);
                    $all_engine_data[$data[4]] = array($data[0] ,$data[1],$data[2],$data[3],$data[4],$data[5],$data[6],$data[7],$data[8],$data[9],$data[10]);
                }
                ++$j;
            }
            $all_brand = array_unique($all_brand);
            $all_model = array_unique($all_model , SORT_REGULAR);
            $all_generation = array_unique($all_generation , SORT_REGULAR);
            $all_engine = array_unique($all_engine);
            $all_engine_data = array_unique($all_engine_data , SORT_REGULAR);

//            echo "<pre>";
//            print_r($all_model);
//            exit;
            insert_brand_dump($all_brand);
            insert_model_dump($all_model);
            insert_generation_dump($all_generation);
            $all_brand = get_all_brand_names_array();
            $all_generation = get_all_generation_names_array();
            $all_model = get_all_model_names_array();
            $i = 0;
            $new_all_engine_data = array();
            foreach ($all_engine_data as $key => $value){
                $brand  = array_search($value[1], $all_brand);
                $model = array_search($value[2], $all_model);
                $gen    = array_search($value[3], $all_generation);
                $new_all_engine_data[$value[4]] = array($value[0] ,$brand ,$model,$gen,$value[4],$value[5],$value[6],$value[7],$value[8],$value[9],$value[10]);
                ++$i;
            }
            //echo $i;
            insert_engine_dump($new_all_engine_data);
            //echo "</table>";
            echo "<h2> Database Imported Successfully  </h2>";

        } else {
            echo "This Is not valid XLSX file";
            exit;
        }
    } else {
        echo $movefile['error'];
        exit;
    }

}



if(!isset($_POST['upload_xslx']) && ! isset($_POST['export_database_submit'])) {
?>


<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" type="text/css">

<style>
    a.dt-button {
        padding: 6px;
        border: 2px solid black;
        display: inline-block;
        margin: 2px 4px;
        background-color: #FFF;
        box-shadow: 4px 4px 4px #CCC;
    }

    div#example_filter {
        display: inline-block;
        float: right;
        margin: 0px 80px;
    }

    .dt-buttons {
        display: inline-block;
    }

    tr.odd {
        background-color: white;
    }

    a#example_previous , a#example_next {
        padding: 6px;
        border: 2px solid black;
        display: inline-block;
        margin: 2px 4px;
        background-color: #FFF;
        box-shadow: 4px 4px 4px #CCC;
        border-radius: 6px;
    }

    a.paginate_button {
        padding: 10px;
    }

    td {
        font-family: inherit;
        font-size: inherit;
        font-weight: inherit;
        line-height: inherit;
        padding: 6px 0px;
        border-bottom: 1px solid #7e7c7c;
    }

    th {
        text-align: left;
        background-color: black;
        /* padding: 10px; */
        /* box-shadow: 20px 20px 20px #CCC; */
        color: white;
        padding: 10px;
        border: 1px dashed #CCC;
    }

</style>

<form class="upload_dump_form" id="upload_dump_form" enctype="multipart/form-data" method="post">
    <input type="file" name="upload_sheet" id="upload_sheet" class="upload_sheet" required>
    <button type="submit" name="upload_xslx">Import Database From Excel File </button>

</form>
<!--
<a href="<?php echo plugins_url('custom_car/01simple-download-xlsx.php')?>"  target="_blank"><button>Export Database </button> </a>
-->
<?php
    global $wpdb;
    $table_engine = $wpdb->prefix .'engine';
    $all_engines = $wpdb->get_results("SELECT * FROM $table_engine"); ?>
    <table id="example" class="display nowrap" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Product Name </th>
            <th>Brand</th>
            <th>Model</th>
            <th>Generation</th>
            <th>Engine</th>
            <th>KM Standard</th>
            <th>KM Tuning</th>
            <th>Nm Standard</th>
            <th>NM Tuning</th>
            <th>KM Box Tuning</th>
            <th>NM Box Tuning</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>Product Name </th>
            <th>Brand</th>
            <th>Model</th>
            <th>Generation</th>
            <th>Engine</th>
            <th>Km Standard</th>
            <th>Km Tuning</th>
            <th>Nm Standard</th>
            <th>NM Tuning</th>
            <th>Km Box Tuning</th>
            <th>NM Box Tuning</th>
        </tr>
        </tfoot>
        <tbody>

        <?php foreach ($all_engines as $engine){ ?>

        <tr>
            <td><?php echo get_brand_name_by_id($engine->b_id)." ".$engine->e_name ?></td>
            <td><?php echo get_brand_name_by_id($engine->b_id) ?></td>
            <td><?php echo  get_model_name_by_id($engine->m_id);  ?></td>
            <td><?php echo get_generation_name_by_id($engine->g_id);  ?></td>
            <td><?php echo $engine->e_name ?></td>
            <td><?php echo $engine->e_km_standard; ?></td>
            <td><?php echo $engine->e_km_tuning ?></td>
            <td><?php echo $engine->e_nm_standard ?></td>
            <td><?php echo $engine->e_nm_tuning ?></td>
            <td><?php echo $engine->e_km_box ?></td>
            <td><?php echo $engine->e_nm_box ?></td>
        </tr>

        <?php         } ?>

        </tbody>
    </table>

    <script src="//code.jquery.com/jquery-1.12.3.js"> </script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"> </script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"> </script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"> </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"> </script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"> </script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"> </script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"> </script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"> </script>


<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel' , 'print'
            ]
        } );
    } );
</script>


    <?php  //export_database()?>
<?php } ?>

<!--
<table style="">
    <?php





/*    $file_ext  = strtolower(substr($file_name, strrpos($file_name, '.')+1));

if($file_ext == "csv")
    {
        while (($data = fgetcsv($file, 1000, ",")) !== FALSE) {
            ?>

            <tr>
                <td ><?php echo  $data[0]; ?></td>
                <td ><?php echo  $data[1]; ?></td>
            </tr>
            <?php
        }
    }
    else // xslx
    {
        include 'simplexlsx.class.php';
        $xlsx = new SimpleXLSX($file_name);

        foreach($xlsx->rows() as $data)
        {
            ?>

            <tr>
                <td ><?php echo  $data[0]; ?></td>
                <td ><?php echo  $data[1]; ?></td>
            </tr>
            <?php
        }

    }
*/
    ?>

</table>

-->