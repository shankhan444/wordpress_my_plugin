jQuery(document).ready(function ($) {
    function show_loader() {
        jQuery('#loader-wrapper').waitMe({
            effect : 'win8_linear',
            text : 'Loading...',
            color : '#000',
            bg: 'rgba(255,255,255,0.5)',
            textPos : 'horizontal',
            opacity:0.5
        });
    }


    $('.b_id').change(function () {
        show_loader();
        $('.m_id').empty();
        $('.g_id').empty();
        $('.e_id').empty();
        $('.g_id').hide();
        $('.e_id').hide();
        $('.m_id').show();
        $('.m_id').prop("disabled", false);
        $('.g_id').prop("disabled", true);
        $('.e_id').prop("disabled", true);
        $('.g_id').append('<option value=""> Select Generation</option>');
        $('.e_id').append('<option value=""> Select Engine</option>');
        $('.g_id').show();
        $('.e_id').show();
        if($('.b_id').val() == ""){
            alert('Please Select Value');
           $('#loader-wrapper').waitMe('hide');
            return ;
        }



        var data = {
            'action': 'get_model_by_brand',
            'brand_id': $('.b_id').val()     // We pass php values differently!
        };
        $.ajax({
            url : $('.ajax_path').val(),
            method : 'post',
            data : data ,
            success : function (response) {
                console.log(response);

                response = response.slice(0,-1);
                console.log(response);


                //response = JSON.parse(response);
                response = $.parseJSON(response);

/*
                    var dataArray = [];
                    for (year in response) {
                        var word = response[year];
                        dataArray.push({id: parseInt(year), value: word});
                    }

                    dataArray.sort(function(a, b){
                        if (a.word < b.word) return -1;
                        if (b.word < a.word) return 1;
                        return 0;
                    });
*/
                //console.log(response);
                $('.m_id').append('<option value=""> Select Model</option>');
                $.each(response , function (model_index , model_name) {
                   $('.m_id').append('<option value="'+model_name+'">' + model_index +'</option>');
                });
               $('#loader-wrapper').waitMe('hide');
            }
        });
    });

    $('.m_id').change(function () {
        show_loader();
        $('.g_id').empty();
        $('.e_id').empty();
        $('.e_id').hide();
        $('.g_id').show();
        $('.g_id').prop("disabled", false);
        $('.e_id').prop("disabled", true);
        $('.e_id').append('<option value=""> Select Engine</option>');
        $('.e_id').show();

        if($('.m_id').val() == ""){
            alert('Please Select Value');
           $('#loader-wrapper').waitMe('hide');
            return ;
        }


        var data = {
            'action': 'get_generation_by_brand_and_model',
            'brand_id': $('.b_id').val(),     // We pass php values differently!
            'model_id': $('.m_id').val()     // We pass php values differently!
        };
        $.ajax({
            url : $('.ajax_path').val(),
            method : 'post',
            data : data ,
            success : function (response) {
                console.log(response);

                response = response.slice(0,-1);
                console.log(response);

                response = JSON.parse(response);
                //console.log(response);
                $('.g_id').append('<option value=""> Select Generation </option>');
                $.each(response , function (generation_name , generation_index) {
                    $('.g_id').append('<option value="'+generation_index+'">' + generation_name +'</option>')
                });
               $('#loader-wrapper').waitMe('hide');
            }
        });

    });


    $('.g_id').change(function () {
        show_loader();
        $('.e_id').empty();
        $('.e_id').prop("disabled", false);
        $('.e_id').show();
        if($('.g_id').val() == ""){
            alert('Please Select Value');
           $('#loader-wrapper').waitMe('hide');
            return ;
        }



        var data = {
            'action': 'get_engine_by_brand_model_and_generation',
            'brand_id': $('.b_id').val(),     // We pass php values differently!
            'model_id': $('.m_id').val(),     // We pass php values differently!
            'generation_id': $('.g_id').val()     // We pass php values differently!
        };
        $.ajax({
            url : $('.ajax_path').val(),
            method : 'post',
            data : data ,
            success : function (response) {
                console.log(response);

                response = response.slice(0,-1);
                console.log(response);
                response = JSON.parse(response);
                //console.log(response);
                $('.e_id').append('<option value=""> Select Engine </option>');
                $.each(response , function (engine_name , engine_index) {
                    $('.e_id').append('<option value="'+engine_index+'">' + engine_name +'</option>')
                });
               $('#loader-wrapper').waitMe('hide');
            }
        });

    });

    $('.e_id').change(function (){
        $(".result_btn").prop("disabled", false);
    });

    $('.result_btn').click (function () {
        $('.result').empty();
        $('.result').show();
        if($('.e_id').val() == "")
            return ;

        var data = {
            'action': 'get_engine_details_by_brand_model_and_generation_and_engine',
            'brand_id': $('.b_id').val(),     // We pass php values differently!
            'model_id': $('.m_id').val(),     // We pass php values differently!
            'generation_id': $('.g_id').val(),     // We pass php values differently!
            'engine_id': $('.e_id').val()     // We pass php values differently!
        };
        $.ajax({
            url :$('.ajax_path').val(),
            method : 'post',
            data : data ,
            success : function (response) {
                console.log(response);
                response = response.slice(0,-1);
                console.log(response);
                $('.result').append(response+'<br/>');
                /*response = JSON.parse(response);
                 $.each(response , function (engine_index , engine_details) {
                 $('.result').append(engine_details+'<br/>');
                 });
                 */
            }
        });

    });

    $(".b_id , .m_id , .g_id , .e_id  ").change(function(){
        if( $(".e_id").val() == "" || $(".e_id").val() == null){
            $(".result_btn").prop("disabled", true);

        }
    });

    /*
     $('.b_id , .m_id , .g_id , .e_id').change(function(){
     $(this).trigger('click');
     });
     */
});