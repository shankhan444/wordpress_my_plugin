<?php
/**
 * Created by PhpStorm.
 * User: SHAN
 * Date: 7/20/2016
 * Time: 7:45 PM
 */

function export_database(){

    ob_start();
    ob_flush();
    ob_clean();
    //exit();
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    date_default_timezone_set('Europe/London');

    if (PHP_SAPI == 'cli')
        die('This example should only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/PHPExcel-1.8/Classes/PHPExcel.php';


// Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

// Set document properties
    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
        ->setLastModifiedBy("Maarten Balliauw")
        ->setTitle("Office 2007 XLSX Test Document")
        ->setSubject("Office 2007 XLSX Test Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Test result file");

    for($i = 1 ; $i < 3; $i++){


// Add some data
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, 'Hello')
            ->setCellValue('B'.$i, 'world!')
            ->setCellValue('C'.$i, 'Hello')
            ->setCellValue('D'.$i, 'world!');

// Miscellaneous glyphs, UTF-8
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, 'Miscellaneous glyphs')
            ->setCellValue('A'.$i, 'éàèùâêîôûëïüÿäöüç');

    }
// Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('Simple');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="01simple.xlsx"');
    header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;

}

function get_brand_name_by_id($brand_id){
	global $wpdb;
	$table_brand  = $wpdb->prefix .'brand';
	$all_brand = $wpdb->get_results("SELECT * FROM $table_brand WHERE b_id = $brand_id " , true );
	//print_r($all_brand);
    if(empty($all_brand)){
        return "N/A";
    }
    else{
        return $all_brand[0]->b_name;
    }

    /*

	   if(is_array($all_brand)){
	     return $all_brand[0]->b_name;
     }
     else{
         return "N/A";
     }
	*/
}

function get_model_name_by_id($model_id){
	global $wpdb;
	$table_model  = $wpdb->prefix .'model';
	$model = $wpdb->get_results("SELECT * FROM $table_model WHERE m_id = $model_id " , true );
	return $model[0]->m_name;
}

function get_generation_name_by_id($generation_id){
	global $wpdb;
	$table_generation  = $wpdb->prefix .'generation';
	$generation = $wpdb->get_results("SELECT * FROM $table_generation WHERE g_id = $generation_id " , true );
	return $generation[0]->g_name;
}

function get_engine_name_by_id($engine_id){
	global $wpdb;
	$table_engine  = $wpdb->prefix .'engine';
	$engine = $wpdb->get_results("SELECT * FROM $table_engine WHERE e_id = $engine_id " , true );
	return $engine[0]->e_name;
}

function get_id_by_brand_name($brand_name){
    global $wpdb;
    $table_brand  = $wpdb->prefix .'brand';
    $all_brand = $wpdb->get_results("SELECT * FROM $table_brand WHERE b_name = '$brand_name' " , true );
    return $all_brand[0]->b_id;
}

function get_id_by_model_name($model_name){
    global $wpdb;
    $table_model  = $wpdb->prefix .'model';
    $model = $wpdb->get_results("SELECT * FROM $table_model WHERE m_name like '$model_name' " , true );
    return $model[0]->m_id;
    //return "ABCD";
}

function get_id_by_generation_name($generation_name){
    global $wpdb;
    $table_generation  = $wpdb->prefix .'generation';
    $generation = $wpdb->get_results("SELECT * FROM $table_generation WHERE g_name = '$generation_name' " , true );
    return $generation[0]->g_id;
}

function get_id_by_engine_name($engine_name){
    global $wpdb;
    $table_engine  = $wpdb->prefix .'engine';
    $engine = $wpdb->get_results("SELECT * FROM $table_engine WHERE e_name = '$engine_name' " , true );
    return $engine[0]->e_id;
}

function get_all_brand(){
	global $wpdb;

	$table_brand  = $wpdb->prefix .'brand';
	$all_brand = $wpdb->get_results("SELECT * FROM $table_brand  " );

	return $all_brand;

}

function get_all_model($brand_id = null){
	global $wpdb;
	$table_model  = $wpdb->prefix .'model';
	if($brand_id == null)
		$all_model = $wpdb->get_results("SELECT * FROM $table_model ORDER BY `m_name` " );
	else
		$all_model = $wpdb->get_results("SELECT * FROM $table_model WHERE `b_id` = $brand_id  ORDER BY `m_name`" );
	return $all_model;
}

function get_all_generation($brand_id = null , $model_id = null){
	global $wpdb;

	$table_generation  = $wpdb->prefix .'generation';
	if($brand_id == null)
		$all_generation = $wpdb->get_results("SELECT * FROM $table_generation  ORDER BY `g_name`" );
	else
		$all_generation = $wpdb->get_results("SELECT * FROM $table_generation WHERE `b_id` = $brand_id AND  `m_id` = $model_id  ORDER BY `g_name`" );
	return $all_generation;

}

function get_all_engine($brand_id = null , $model_id = null , $generation_id = null){
	global $wpdb;
	$table_engine  = $wpdb->prefix .'engine';
	if($brand_id == null)
		$all_engine = $wpdb->get_results("SELECT * FROM $table_engine  ORDER BY `e_name`" );
	else
		$all_engine = $wpdb->get_results("SELECT * FROM  WHERE `b_id` = $brand_id AND  `m_id` = $model_id AND `g_id` = $generation_id  ORDER BY `e_name` " );
	return $all_engine;
}

function get_all_brand_names_array(){
    global $wpdb;

    $table_brand  = $wpdb->prefix .'brand';
    $all_brand = $wpdb->get_results("SELECT * FROM $table_brand" );
    $brand_name_array = array();

    foreach ($all_brand as $brand){
        $brand_name_array[$brand->b_id] = $brand->b_name;
    }

    return $brand_name_array;

}

function get_all_generation_names_array(){
    global $wpdb;
    $table_generation  = $wpdb->prefix .'generation';
    $all_generation = $wpdb->get_results("SELECT * FROM $table_generation" );
    $generation_name_array = array();
    foreach ($all_generation as $generation){
        $generation_name_array[$generation->g_id] = $generation->g_name;
    }
    return $generation_name_array;
}

function get_all_model_names_array(){
    global $wpdb;

    $table_model  = $wpdb->prefix .'model';
    $all_model = $wpdb->get_results("SELECT * FROM $table_model" );
    $model_name_array = array();

    foreach ($all_model as $model){
        $model_name_array[$model->m_id] = $model->m_name;
    }

    return $model_name_array;

}

function get_all_engine_names_array(){
    global $wpdb;

    $table_engine  = $wpdb->prefix .'engine';
    $all_engine = $wpdb->get_results("SELECT * FROM $table_engine" );
    $engine_name_array = array();

    foreach ($all_engine as $engine){
        $engine_name_array[$engine->e_id] = $engine->e_name;
    }

    return $engine_name_array;

}

function insert_brand_dump($array_of_brands){
    global $wpdb;
    $table_brand = $wpdb->prefix .'brand';

    $exist_brand = get_all_brand_names_array();
    foreach ($array_of_brands as $key => $brand_name){

        if(! in_array($brand_name , $exist_brand)){
            $data_array = array(
                'b_name' => $brand_name,
            );
            $wpdb->insert($table_brand ,$data_array);
        }

    }

}

function insert_model_dump($array_of_model){
    //echo "<pre>";
    //print_r($array_of_model);
    global $wpdb;
    $table_model = $wpdb->prefix .'model';
    $exist_model = get_all_model_names_array();
    foreach ($array_of_model as $model_name){
        if(! in_array($model_name[1] , $exist_model)){
            $data_array = array(
                'b_id' => get_id_by_brand_name($model_name[0]),
                'm_name' => $model_name[1],
            );
            $wpdb->insert($table_model ,$data_array);
        }

    }


}

function insert_generation_dump($array_of_generation){
    global $wpdb;
    $table_generation = $wpdb->prefix .'generation';

    $exist_generation = get_all_generation_names_array();

    foreach ($array_of_generation as $generation_name){

        if(! in_array($generation_name[2] , $exist_generation)){
            $data_array = array(
                'b_id' => get_id_by_brand_name($generation_name[0]),
                'm_id' => get_id_by_model_name($generation_name[1]),
                'g_name' => $generation_name[2],
            );
            $wpdb->insert($table_generation ,$data_array);
        }

    }

}

function insert_engine_dump($array_of_engine){
    global $wpdb;
    $table_engine = $wpdb->prefix .'engine';
    $exist_engine = get_all_engine_names_array();
    //$exist_engine = array();
    foreach ($array_of_engine as $key => $engine_name){
        if(! in_array($engine_name[4] , $exist_engine)){
            $data_array = array(
                'b_id' => $engine_name[1],
                'm_id' => $engine_name[2],
                'g_id' => $engine_name[3],
                'e_name' => $engine_name[4],
                'e_km_standard' => $engine_name[5],
                'e_km_tuning' => $engine_name[6],
                'e_nm_standard' => $engine_name[7],
                'e_nm_tuning' => $engine_name[8],
                'e_km_box' => $engine_name[9],
                'e_nm_box' => $engine_name[10],
            );
            $wpdb->insert($table_engine ,$data_array);
        }
    }

}

