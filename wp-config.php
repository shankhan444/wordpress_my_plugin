<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_my_plugin');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ' LW|_}k/YTR{^CA0WV=9%jvTVuHu=/6#a&!-DdfB_HcFLy-sjk}+g4R{p8it:lL*');
define('SECURE_AUTH_KEY',  'd9QQ3@`)NE{A4l!?3`UPa}sZ`+>Zf^Pcfj0,QEy>To#5?sC{OhJr+VE^$i ~-y%;');
define('LOGGED_IN_KEY',    'KjX=0e5va2m255L>g2};KOhER[AzKelI@qH?>]e$mr<WE]5~O2M]([$2|P$[R{T>');
define('NONCE_KEY',        '5zVkWHpOB{d%#@U_S$7*8-r?)!`5goEM(xk)zaP:j|L5o:.qN86AP;d-w#cah&QT');
define('AUTH_SALT',        '/5mD>ie;!9Jy(IuwEXX;mUZh=(ky[?l4>uf,Y>_3kcDwmp>|`=<@=sA9X`dc16&d');
define('SECURE_AUTH_SALT', 'r:djm?D</Hy[z?28b]yBY]Cmr<.2X5%o7;p69IG@~J?aM{}- pXHD?ss3f<%bU`?');
define('LOGGED_IN_SALT',   ' (-FV55{_zu_GzqzTQ|9~K!dnA=9 C|:~ja6)!XEYeR[.>A<NYcilqREzqgulKH`');
define('NONCE_SALT',       ':pX+h~l%(_+yvdyr]c?Z_1gP@ibnL4L(Wo[1mp:5MZv&w^b?6>3+_u5EV!-d|Q-6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
//define('WP_DEBUG', false);
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
